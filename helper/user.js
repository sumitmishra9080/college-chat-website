let users = [];

module.exports = {
    addUser: (user) => {
        username = user.username.trim().toLowerCase();
        room = user.room.trim().toLowerCase();

        if (!username || !room) {
            return {
                error: "Username or room not specified",
            };
        }

        const existingUser = users.find((element) => {
            return element.room === room && element.username === username;
        });

        if (existingUser) {
            return {
                error: "User already exists",
            };
        }
        const userDetails = { id: user.id, username, room };
        users.push(userDetails);
        return { userDetails: userDetails };
    },
};
