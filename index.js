const path = require("path");
const http = require("http");
const express = require("express");
const socketio = require("socket.io");
const { addUser } = require("./helper/user");

const app = express();
const server = http.createServer(app);
const io = socketio(server);

require("dotenv").config();

const generateMessage = (username, message) => {
    return {
        username,
        message,
        createdAt: new Date().getTime(),
    };
};
const port = process.env.PORT || 3000;
const publicDirectoryPath = path.join(__dirname, "../public");

app.use(express.static(publicDirectoryPath));
app.set("view engine", "ejs");

app.get("/", (req, res) => {
    return res.render("index.ejs");
});
app.get("/chat", (req, res) => {
    const query = req.query;
    return res.render("chat_page.ejs", { username: query.username, room: query.room });
});
io.on("connection", (socket) => {
    console.log("New WebSocket connection", socket.id);

    socket.on("join", (details, callback) => {
        console.log("joined details", { ...details });
        const { error, userDetails } = addUser({ id: socket.id, ...details });
        if (error) {
            console.log("Error: ", error);
            return callback(error);
        } else {
            console.log("userDetails", userDetails);
            socket.join(userDetails.room);
            console.log("message emit");
            console.log("user", generateMessage("Admin", "Welcome!"));
            socket.emit("message", generateMessage("Admin", "Welcome!"));
            console.log("userDetails", userDetails);
            socket.broadcast.to(userDetails.room).emit("message", generateMessage("Admin", `${userDetails.username} has joined!`));
            callback();
        }
    });
    socket.on("send-message", (data) => {
        console.log("data", data);
        io.to(data.room).emit("message", generateMessage(data.username, data.msg));
    });

    // io.to(userDetails.room).emit("roomData", {
    //     room: userDetails.room,
    //     users: getUsersInRoom(userDetails.room),
    // });
});

server.listen(port, () => {
    console.log(`Server is up on port ${port}!`);
});
